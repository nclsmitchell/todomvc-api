namespace :deploy do
    task :run do
        on roles(:all) do
            execute "cd /var/www/todomvc-api/current && pm2 start dist/server.js"
        end
    end
end