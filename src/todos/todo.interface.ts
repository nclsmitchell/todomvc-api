import { Document } from 'mongoose';

export interface CreateTodoDto extends Document {
  readonly title: string;
}

export interface Todo extends Document {
  readonly title: string;
  readonly completed: boolean;
}