import { Body, Controller, Delete, Get, HttpCode, Param, Post } from '@nestjs/common';
import { TodosService } from './todos.service';
import { CreateTodoDto, Todo } from './todo.interface';


@Controller('todos')
export class TodosController {
  constructor(private readonly todoService: TodosService) {}

  @Get()
  getTodos(): Promise<Todo[]>  {
    return this.todoService.getTodos();
  }

  @Post()
  createTodos(
    @Body() createTodoDto: CreateTodoDto
    ): Promise<Todo>  {
    return this.todoService.createTodo(createTodoDto);
  }

  @Delete(':id')
  @HttpCode(204)
  removeTodo(
    @Param('id') id: string
  ): Promise<{}> {
    return this.todoService.removeTodo(id);
  }
}
