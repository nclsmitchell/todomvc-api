import { Model } from 'mongoose';
import { Inject, Injectable, Response } from '@nestjs/common';
import { CreateTodoDto, Todo } from './todo.interface';


@Injectable()
export class TodosService {
  constructor(
    @Inject('TodoModelToken')
    private readonly todoModel: Model<Todo>,
  ) {}

  async getTodos(): Promise<Todo[]> {
    return await this.todoModel.find().exec();
  }

  async createTodo(createTodoDto: CreateTodoDto): Promise<Todo> {
    const createdTodo = new this.todoModel(createTodoDto);
    return await createdTodo.save();
  }

  async removeTodo(id: string): Promise<{}> {
      return await this.todoModel.findByIdAndDelete(id);
  }
}
