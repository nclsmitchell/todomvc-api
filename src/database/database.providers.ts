import * as mongoose from 'mongoose';

export const databaseProviders = [
  {
    provide: 'DbConnectionToken',
    useFactory: async (): Promise<typeof mongoose> =>
      await mongoose.connect('mongodb://nclsmitchell:GmanG1pom@ds125322.mlab.com:25322/todomvc'),
  },
];